<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>RMS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('app_assets/assets/images/favicon.png')}}">
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{asset('app_assets/assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet')}}">
    <!-- Custom Stylesheet -->
    <link href="{{asset('app_assets/css/style.css')}}" rel="stylesheet">

</head>
<!--**********************************
    Header start
***********************************-->
<div class="header">
    <div class="header-content clearfix">
        <div class="header-left">
            <div class="input-group icons">
                <div class="input-group-prepend">
                    <span class="input-group-text bg-transparent border-0" id="basic-addon1"><i class="icon-magnifier"></i></span>
                </div>
                <input type="search" class="border-0" placeholder="Search here">
                <div class="drop-down animated flipInX d-md-none">
                    <form action="#">
                        <input type="text" class="form-control" placeholder="Search">
                    </form>
                </div>
            </div>
        </div>
        <div class="header-right">

            <ul class="clearfix">
                <li class="icons d-none d-md-flex">
                    <a href="javascript:void(0)" class="window_fullscreen-x">
                        <i class="icon-frame"></i>
                    </a>
                </li>
                <li class="icons">
                    <a href="javascript:void(0)" class="">
                        <i class="icon-envelope"></i>
                        <span class="badge badge-danger">3</span>
                    </a>
                    <div class="drop-down animated flipInX">
                        <div class="dropdown-content-body">
                            <ul>
                                <li class="notification-unread">
                                    <a href="javascript:void()">
                                        <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/1.jpg')}}" alt="">
                                        <div class="notification-content">
                                            <div class="notification-text">Hey, What's up! You have a good news !!!</div>
                                            <div class="notification-timestamp">08 Hours ago</div>
                                        </div>
                                    </a>
                                </li>
                                <li class="notification-unread">
                                    <a href="javascript:void()">
                                        <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/2.jpg')}}" alt="">
                                        <div class="notification-content">
                                            <div class="notification-timestamp">08 Hours ago</div>
                                            <div class="notification-text">Can you do me a favour?</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/3.jpg')}}" alt="">
                                        <div class="notification-content">
                                            <div class="notification-text">Hey!</div>
                                            <div class="notification-timestamp">08 Hours ago</div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/4.jpg')}}" alt="">
                                        <div class="notification-content">
                                            <div class="notification-text">And what do you do?</div>
                                            <div class="notification-timestamp">08 Hours ago</div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <a class="d-flex justify-content-center bg-primary px-4 text-white" href="email-inbox.html">
                                <span>See all messagese </span>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="icons">
                    <a href="javascript:void(0)" class="">
                        <i class="icon-bell"></i>
                        <span class="badge badge-primary">3</span>
                    </a>
                    <div class="drop-down animated flipInX dropdown-notfication">
                        <div class="dropdown-content-body">
                            <ul>
                                <li>
                                    <a href="javascript:void()">
                                        <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-calender"></i></span>
                                        <div class="notification-content">
                                            <h6 class="notification-heading">Event Started</h6>
                                            <span class="notification-text">One hour ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <span class="mr-3 avatar-icon bg-danger-lighten-2"><i class="icon-calender"></i></span>
                                        <div class="notification-content">
                                            <h6 class="notification-heading">Event Started</h6>
                                            <span class="notification-text">One hour ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-calender"></i></span>
                                        <div class="notification-content">
                                            <h6 class="notification-heading">Event Started</h6>
                                            <span class="notification-text">One hour ago</span>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void()">
                                        <span class="mr-3 avatar-icon bg-danger-lighten-2"><i class="icon-calender"></i></span>
                                        <div class="notification-content">
                                            <h6 class="notification-heading">Event Started</h6>
                                            <span class="notification-text">One hour ago</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <a class="d-flex justify-content-between bg-primary px-4 text-white" href="javascript:void()">
                                <span>All Notifications</span>
                                <span><i class="icon-settings"></i></span>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="icons">
                    <div class="user-img c-pointer-x">
                        <span class="activity active"></span>
                        <img src="{{asset('app_assets/assets/images/user/1.png')}}" height="40" width="40" alt="">
                    </div>
                    <div class="drop-down dropdown-profile animated flipInX">
                        <div class="dropdown-content-body">
                            <ul>
                                <li><a href="javascript:void()"><i class="icon-user"></i> <span>My Profile</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-calender"></i> <span>My Calender</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-envelope-open"></i> <span>My Inbox</span> <div class="badge gradient-3 badge-pill badge-primary">3</div></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-paper-plane"></i> <span>My Tasks</span><div class="badge badge-pill bg-dark">3</div></a>
                                </li>

                                <li><a href="javascript:void()"><i class="icon-check"></i> <span>Online</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-lock"></i> <span>Lock Screen</span></a>
                                </li>
                                <li><a href="javascript:void()"><i class="icon-key"></i> <span>Logout</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>


    </div>
</div>
<!--**********************************
    Header end ti-comment-alt
***********************************-->

<!--**********************************
    Sidebar start
***********************************-->
<div class="nk-sidebar">
    <div class="nk-nav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Dashboard</li>
            <li>
                <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                    <i class="icon-speedometer"></i><span class="nav-text">Menu Master</span>
                </a>
                <ul aria-expanded="false">
                    <li><a href="menumaster.html">Add Menu</a></li>
                    <li><a href="/Table">Tables</a></li>
                    <li><a href="/Currency">Currency</a></li>
                    <li><a href="intro-cart.html">Cart</a></li>
                    <li><a href="intro-checkout.html">Checkout</a></li>
                    <li><a href="restaurant-menus.html">Menus</a></li>
                    <li><a href="restaurant-offers.html">Offer</a></li>
                    <li><a href="restaurant-reservation.html">Reservation</a></li>
                    <li><a href="restaurant-employees.html">Employees</a></li>
                    <li><a href="restaurant-custommer.html">Custommer</a></li>
                    <li><a href="restaurant-order.html">Order</a></li>
                    <li><a href="restaurant-packages.html">Packages</a></li>
                    <li><a href="intro-wishlist.html">Wishlist</a></li>
                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon-layers"></i><span class="nav-text">Layouts</span></a>
                <ul aria-expanded="false">
                    <li><a href="layout-blank.html">Blank</a></li>
                    <li><a href="layout-one-column.html">One Column</a></li>
                    <li><a href="layout-two-column.html">Two column</a></li>
                    <li><a href="layout-fixed-header.html">Fixed Header</a></li>
                    <li><a href="layout-fixed-sidebar.html">Fixed Sidebar</a></li>
                    <li><a href="layout-horizontal-nav.html">Horizontal</a></li>
                    <li><a href="layout-rtl.html">RTL</a></li>
                    <li><a href="layout-boxed.html">Boxed</a></li>
                    <li><a href="layout-wide-boxed.html">Wide Boxed</a></li>
                    <li><a href="layout-wide.html">Wide</a></li>
                    <li><a href="layout-dark.html">Dark</a></li>
                    <li><a href="layout-light.html">Light</a></li>
                </ul>
            </li>
            <li class="nav-label">Apps</li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon-screen-smartphone"></i><span class="nav-text">Apps</span></a>
                <ul aria-expanded="false">
                    <li><a href="email-inbox.html">Mailbox</a></li>
                    <li><a href="app-profile.html">Profile</a></li>
                    <li><a href="app-calender.html">Calendar</a></li>
                </ul>
            </li>
            <li><a href="charts.html" aria-expanded="false"><i class="icon-chart"></i> <span class="nav-text">Charts</span></a></li>
            <li class="nav-label">UI Components</li>
            <li><a href="ui-bootstrap.html" aria-expanded="false"><i class="icon-diamond"></i><span class="nav-text">UI Bootstrap</span></a></li>
            <li><a href="components.html" aria-expanded="false"><i class="icon-puzzle"></i><span class="nav-text">Components</span></a></li>
            <li><a href="widget-basic.html" aria-expanded="false"><i class="icon-badge"></i><span class="nav-text">Widget</span></a></li>
            <li class="nav-label">Forms</li>
            <li><a href="forms.html" aria-expanded="false"><i class="icon-settings"></i><span class="nav-text">Forms</span></a></li>
            <li class="nav-label">Table</li>
            <li><a href="tables.html" aria-expanded="false"><i class="icon-briefcase"></i><span class="nav-text">Table</span></a></li>
            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="icon-globe"></i><span class="nav-text">Pages</span></a>
                <ul aria-expanded="false">
                    <li><a href="page-login.html">Login</a></li>
                    <li><a href="page-register.html">Register</a></li>
                    <li><a href="page-user-lock.html">Lock Screen</a></li>
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false">Erorr</a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="page-error-400.html">Erorr 400</a></li>
                            <li><a href="page-error-403.html">Erorr 403</a></li>
                            <li><a href="page-error-404.html">Erorr 404</a></li>
                            <li><a href="page-error-500.html">Erorr 500</a></li>
                            <li><a href="page-error-503.html">Erorr 503</a></li>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</div>
<!--**********************************
    Sidebar end
***********************************-->

<!--**********************************
    Content body start
***********************************-->
