<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Portion;
use App\Models\Mode;
use App\Models\Menurate;
use App\Models\Menu;
class MenurateController
{
  public function index()
  {
   //Menu::create($request->except('_token'));
   $portions=Portion::select('portion_name')->get();
   $modes=Mode::select('Mode_name')->get();
   $menu=Menu::select('menu_name')->get();
   return view('menurate',compact('portions','modes','menu'));

  }
    //
}
