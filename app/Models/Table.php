<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
  protected $primaryKey = 'table_id';
protected $fillable = [
    'table_no','seats', 'status',
  ];
    //
}
