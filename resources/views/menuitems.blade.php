<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>RMS</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('app_assets/assets/images/favicon.png')}}">
    <!-- JS Grid -->
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/jsgrid/css/jsgrid.min.css')}}">
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/jsgrid/css/jsgrid-theme.min.css')}}">
    <!-- Footable -->
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/footable/css/footable.bootstrap.min.css')}}">
    <!-- Bootgrid -->
    <link rel="stylesheet" href="{{asset('app_assets/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.css')}}">
    <!-- Datatable -->
    <link href="{{asset('app_assets/assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet')}}">
    <!-- Custom Stylesheet -->
    <link href="{{asset('app_assets/css/style.css')}}" rel="stylesheet">

</head>

<body>

    <div id="preloader">
        <div class="loader"></div>
    </div>
  
    <div id="main-wrapper">

        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr">D</b>
                    <span class="brand-title"><b>RMS</b></span>
                </a>
            </div>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="toggle-icon"><i class="icon-menu"></i></span>
                </div>
            </div>
        </div>
       
        <div class="header">
            <div class="header-content clearfix">
                <div class="header-left">
                    <div class="input-group icons">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent border-0" id="basic-addon1"><i class="icon-magnifier"></i></span>
                        </div>
                        <input type="search" class="border-0" placeholder="Search here">
                        <div class="drop-down animated flipInX d-md-none">
                            <form action="#">
                                <input type="text" class="form-control" placeholder="Search">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="header-right">

                    <ul class="clearfix">
                        <li class="icons d-none d-md-flex">
                            <a href="javascript:void(0)" class="window_fullscreen-x">
                                <i class="icon-frame"></i>
                            </a>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)" class="">
                                <i class="icon-envelope"></i>
                                <span class="badge badge-danger">3</span>
                            </a>
                            <div class="drop-down animated flipInX">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li class="notification-unread">
                                            <a href="javascript:void()">
                                                <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/1.jpg')}}" alt="">
                                                <div class="notification-content">
                                                    <div class="notification-text">Hey, What's up! You have a good news !!!</div>
                                                    <div class="notification-timestamp">08 Hours ago</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="notification-unread">
                                            <a href="javascript:void()">
                                                <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/2.jpg')}}" alt="">
                                                <div class="notification-content">
                                                    <div class="notification-timestamp">08 Hours ago</div>
                                                    <div class="notification-text">Can you do me a favour?</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void()">
                                                <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/3.jpg')}}" alt="">
                                                <div class="notification-content">
                                                    <div class="notification-text">Hey!</div>
                                                    <div class="notification-timestamp">08 Hours ago</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void()">
                                                <img class="float-left mr-3 avatar-img" src="{{asset('app_assets/assets/images/avatar/4.jpg')}}" alt="">
                                                <div class="notification-content">
                                                    <div class="notification-text">And what do you do?</div>
                                                    <div class="notification-timestamp">08 Hours ago</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <a class="d-flex justify-content-center bg-primary px-4 text-white" href="email-inbox.html">
                                        <span>See all messagese </span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <a href="javascript:void(0)" class="">
                                <i class="icon-bell"></i>
                                <span class="badge badge-primary">3</span>
                            </a>
                            <div class="drop-down animated flipInX dropdown-notfication">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="javascript:void()">
                                                <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-calender"></i></span>
                                                <div class="notification-content">
                                                    <h6 class="notification-heading">Event Started</h6>
                                                    <span class="notification-text">One hour ago</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void()">
                                                <span class="mr-3 avatar-icon bg-danger-lighten-2"><i class="icon-calender"></i></span>
                                                <div class="notification-content">
                                                    <h6 class="notification-heading">Event Started</h6>
                                                    <span class="notification-text">One hour ago</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void()">
                                                <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-calender"></i></span>
                                                <div class="notification-content">
                                                    <h6 class="notification-heading">Event Started</h6>
                                                    <span class="notification-text">One hour ago</span>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void()">
                                                <span class="mr-3 avatar-icon bg-danger-lighten-2"><i class="icon-calender"></i></span>
                                                <div class="notification-content">
                                                    <h6 class="notification-heading">Event Started</h6>
                                                    <span class="notification-text">One hour ago</span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <a class="d-flex justify-content-between bg-primary px-4 text-white" href="javascript:void()">
                                        <span>All Notifications</span>
                                        <span><i class="icon-settings"></i></span>
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li class="icons">
                            <div class="user-img c-pointer-x">
                                <span class="activity active"></span>
                                <img src="{{asset('app_assets/assets/images/user/1.png')}}" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile animated flipInX">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li><a href="javascript:void()"><i class="icon-user"></i> <span>My Profile</span></a>
                                        </li>
                                        <li><a href="javascript:void()"><i class="icon-calender"></i> <span>My Calender</span></a>
                                        </li>
                                        <li><a href="javascript:void()"><i class="icon-envelope-open"></i> <span>My Inbox</span> <div class="badge gradient-3 badge-pill badge-primary">3</div></a>
                                        </li>
                                        <li><a href="javascript:void()"><i class="icon-paper-plane"></i> <span>My Tasks</span><div class="badge badge-pill bg-dark">3</div></a>
                                        </li>

                                        <li><a href="javascript:void()"><i class="icon-check"></i> <span>Online</span></a>
                                        </li>
                                        <li><a href="javascript:void()"><i class="icon-lock"></i> <span>Lock Screen</span></a>
                                        </li>
                                        <li><a href="javascript:void()"><i class="icon-key"></i> <span>Logout</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
 
        <div class="nk-sidebar">
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Dashboard</li>
				
                    <li>
                        <a class="has-arrow" href="javascript:void()" aria-expanded="false">
                            <i class="icon-speedometer"></i><span class="nav-text">Menu Master</span>
                        </a>
                        <ul aria-expanded="false">
							  <li><a href="/meuitems" class="leftSec">MenuItem</a></li>
                            <li><a href="/kitchen_type" class="leftSec">Kitchen Type</a></li>
                                      <li><a href="/food_category" class="leftSec">Food Category</a></li>
                                      <li><a href="/Portion" class="leftSec">Portion</a></li>
                                      <li><a href="/Mode" class="leftSec">Mode Of Operation</a></li>
                                      <li><a href="/Currency" class="leftSec">Currency Type</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
      
        <div class="content-body">

            <div class="container-fluid">
                <div class="row justify-content-between mb-3">
					<div class="col-12 text-left">
						<h2 class="page-heading"></h2>

					</div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h3 class="content-heading">MENU ITEMS</h3>
                    </div>

 <div class="col-xl-6 col-xxl-12">
                        <div class="card ">
                            <div class="card-body">
                                <h4 class="card-title">Menu Items</h4>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" style="float:Right;margin-bottom: 10px;">Add Menu +</button>
                                <!-- Modal -->
                                <div class="modal fade " id="exampleModalLong" >
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add New Menu</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">
                                                <div class="basic-form">
                                                    <form action="/add_menu" method="post">
                                                               @csrf
                                                        <div class="form-row ">
                                                            <div class="form-group col-md-6">
                                                                <label>Menu Name</label>
                                                                <input type="text" class="form-control"  name="MenuName" placeholder="Menu Name">
                                                            </div>
															<div class="form-group col-md-6">
                                                                <label>Item Code</label>
                                                                <input type="text" class="form-control"  name="ItemCode" placeholder="Item Code">
                                                            </div>
                                                          </div>
                                                          <div class="form-row ">
                                                            <div class="form-group col-md-6">
                                                                <label> Name in Bill</label>
                                                                <input type="text" class="form-control"  name="NameinBill" placeholder="Name in Bill">
                                                            </div>
															<div class="form-group col-md-6">
                                                                <label>Kitchen</label>
                                                                <select class="form-control"  name="Kitchen" placeholder="Kitchen">
																<option value="">select</option>
																<option>Kitchen</option>
																<option>Kitchen</option>
																
																</select>
                                                            </div>
                                                          </div>
														    <div class="form-row ">
                                                            <div class="form-group col-md-6">
                                                                <label>Main Category</label>
                                                                <select class="form-control"  name="MainCategory" placeholder="Main Category">
																<option value="">select</option>
																<option>Category</option>
																<option>Category</option>
																</select>
                                                            </div>
															<div class="form-group col-md-6">
                                                                <label>Diet.</label>
                                                                <select  class="form-control"  name="Diet" placeholder="Diet">
																<option value="">select</option>
																<option>Category</option>
																<option>Category</option>
																</select>
                                                            </div>
                                                          </div>
														    <div class="form-row ">
                                                            <div class="form-group col-md-6">
                                                                <label>Est.Time</label>
</label>
                                                                <input type="text" class="form-control"  name="EstTime" placeholder="Est.Time">
                                                            </div>
															<div class="form-group col-md-6">
                                                                <label>Prepe Mode</label>
                                                                <select class="form-control"  name="PrepeMode" placeholder="Prepe Mode">
																<option>Category</option>
																<option>Category</option>
																</select>
                                                            </div>
                                                          </div>
														    <div class="form-row ">
                                                            <div class="form-group col-md-4">
                                                                <label>Rate Type</label>
                                                                <select  class="form-control"  name="RateType" placeholder="Rate Type">
																<option value="">select</option>
																<option>Category</option>
																<option>Category</option>
																</select>
                                                            </div>
															<div class="form-group col-md-4">
                                                                <label>Base Unit</label>
                                                                <select class="form-control"  name="BaseUnit" placeholder="Base Unit">
																<option value="">select</option>
																<option>Category</option>
																<option>Category</option>
																</select>
                                                            </div>
                                                     
														   
                                                            <div class="form-group col-md-4">
                                                                <label>Unit Type</label>
                                                                <select class="form-control"  name="UnitType" placeholder="Unit Type">
																<option value="">select</option>
																<option>Category</option>
																<option>Category</option>
																</select>
                                                            </div>
															</div>
															 <div class="form-row ">
															<div class="form-group col-md-12">
                                                                <label>Description</label>
</label>
                                                                <textarea class="form-control"  name="Description" placeholder="Description"></textarea>
                                                            </div>
                                                          </div>
														  <div class="form-row ">
                                                            <div class="form-group col-md-4">
                                                               
                                                                <input type="radio" value="No"   name="Active" placeholder="Menu Name">
																 <label>Active</label>
                                                            </div>
															<div class="form-group col-md-4">
                                                               
</label>
                                                                <input type="radio" value="No"   name="AddOns" placeholder="Menu Name">
																 <label>Add Ons</label>
                                                            </div>
															<div class="form-group col-md-4">
                                                               
                                                                <input type="radio" value="No"  name="Stock" placeholder="Menu Name">
																 <label>Stock</label>
                                                            </div>
                                                          </div>
														   <div class="form-row ">
                                                            <div class="form-group col-md-4">
                                                      
                                                                <input type="radio" value="No"  name="DynamicRate" placeholder="Menu Name">
																          <label>Dynamic Rate</label>
                                                            </div>
															<div class="form-group col-md-4">
                                                              
                                                                <input type="radio" value="No"  name="StockNumbers" placeholder="Menu Name">
																  <label>Stock in Numbers</label>
                                                            </div>
															<div class="form-group col-md-4">
                                                               
</label>
                                                                <input type="radio" value="No" name="ShowinKod" placeholder="Menu Name">
																 <label>Show in Kod</label>
                                                            </div>
                                                          </div>
														   <div class="form-row ">
                                                            <div class="form-group col-md-4">
                                                               
</label>
                                                                <input type="radio"  value="No"  name="ExemptTax" placeholder="Menu Name">
																 <label>Exempt Tax</label>
                                                            </div>
															<div class="form-group col-md-4">
                                                                
</label>
                                                                <input type="radio" value="No"  name="PrintKot" placeholder="Menu Name">
																<label>Print in Kot</label>
                                                            </div>
															<div class="form-group col-md-4">
                                                            
                                                                <input type="radio" value="No"  name="ManualBarcode" placeholder="Menu Name">
																    <label>Manual Barcode</label>
                                                            </div>
                                                          </div>
														 
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <input type="submit" value="Save and Proceed" class="btn btn-primary" class="btn btn-primary">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
								   <div class="modal fade " id="exampleModalLong1" >
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Edit Menu Item</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body ">
                                                <div class="basic-form">
                                                  <form action ="/edit_cur" method = "post">
                                                 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                                        <div class="form-row ">
                                                          <div class="form-group col-md-12">
                                                              <label>Id</label>
                                                              <input type="text" class="form-control" id="c_id" name="c_id" placeholder="Menu Name" >
                                                          </div>
                                                            <div class="form-group col-md-12">
                                                                <label>Currency Name</label>
                                                                <input type="text" class="form-control" id="c_type" name="c_type" placeholder="Menu Name">
                                                            </div>
                                                          </div>
                                                          <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label>Country</label>
                                                                <input type="text" class="form-control" name="country" id="country" placeholder="Item Code">
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-primary" class="btn btn-primary">
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

 <div class="table-responsive">
                                    <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                        <thead>
                                            <tr>
                                                <th scope="col">SI.No</th>
                                                <th scope="col">Menu </th>
                                                <th scope="col">Menu Category</th>
                                                <th scope="col">Diet</th>
												<th scope="col">Active</th>
                                                <th scope="col">Show in Kod</th>
                                                <th scope="col">Action</th>
                                                <th scope="col">Rate</th>
												 <th scope="col">Item Tax</th>
                                            </tr>
                                        </thead>
                                        <tbody id="users-crud">
              @foreach ($users as $user)
        <tr>
           <td>{{ $user->id }}</td>
          <td>{{ $user->MenuName }}</td>
                                       <td>{{ $user->Maincategory }}</td>
									   <td>{{ $user->Diet }}</td>
                                       <td>{{ $user->Active }}</td>
                                       <td>{{ $user->ShowinKod }}</td>
									   <td><div class="img-r"><a href= 'edit/{{ $user->id }}' title="Edit"><img src="{{asset('app_assets/images/edit.png')}}"></a></div><div class="img-r"><a href='destroy_p/{{ $user->id }}' onclick="return confirm('Are you sure you want to delete this item?');" title="Delete"><img src="{{asset('app_assets/images/delete.png')}}"></a></div></td>
                                       <td>{{ $user->RateType }}</td>
                                       <td>{{ $user->ExemptTax }}</td>
              @endforeach
              </tr>
            </tbody>
           </table>

                                </div>
                                <!-- Button trigger modal -->
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div></div></div>
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed & Developed by <a href="" target="_blank">otlet.in</a> 2019</p>
            </div>
        </div>
    </div>
  
    <script src="{{asset('app_assets/assets/plugins/common/common.min.js')}}"></script>
    <script src="{{asset('app_assets/js/custom.min.js')}}"></script>
    <script src="{{asset('app_assets/js/settings.js')}}"></script>
    <script src="{{asset('app_assets/js/quixnav.js')}}"></script>
    <script src="{{asset('app_assets/js/styleSwitcher.js')}}"></script>

    <!-- JS Grid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('app_assets/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{asset('app_assets/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{asset('app_assets/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>


    <!-- JS Grid Init -->
    <script src="{{asset('app_assets/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/datatables.init.js')}}"></script>

    <script>
    $(document).ready(function () {
    $('button.open-modal').on('click', function (e) {
        // Make sure the click of the button doesn't perform any action
        e.preventDefault();

        // Get the modal by ID
        var modal = $('#exampleModalLong1');

        // Set the value of the input fields
        modal.find('#c_type').val($(this).data('c_type'));
        modal.find('#country').val($(this).data('country'));
        modal.find('#c_id').val($(this).data('id'));


    });
});

    </script>

</body>
</html>