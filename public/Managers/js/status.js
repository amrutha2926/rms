// JavaScript Document
/*custom scrollbar starts*/
	(function($) {
		$(window).on("load", function() {
			$(".scroll").mCustomScrollbar({
				axis: "y",
				theme: "dark-3"
			});
		});
	})(jQuery);
	/*custom scrollbar ends*/
	/*pagination starts*/
	$(".paginationRight li").click(function(){
		$(this).addClass("active").siblings().removeClass("active");
	});
	/*pagination ends*/
	
	 
	  $(document).ready(function(e) {
			/*select all starts*/  	
			$().chkAll({
				mainClass:'.chkAll', 
				subClass:'.chk',    
				containerClass:".chkgrop"	
			});
			/*select all ends*/	
			/*statusPopOne starts*/	
			$(".statusPopOne .markBtn").click(function(){
				$(".statusPopOne").hide();
				$(".btn").removeClass("grnBtn");
			});
		  	$(".btn").click(function(){
				$(this).toggleClass("grnBtn");
				$(".statusPopOne").show();
			});
		  /*statusPopOne ends*/	
		  /*mappopup starts*/	
		    $(".textOne").click(function(){
				$(".MapPop").show();
				$(".mainBox2").hide();
				$(".mainBox1").show();
			});
			$(".textTwo").click(function(){
				$(".MapPop").show();
				$(".mainOne").show();
				$(".mainBox1").hide();
				$(".mainBox2").show();
				$('.mainTwo').css("display","none");
                $('.mainThree').hide();
				
			});
            $('.blueText').click(function(e) {
                $('.mainOne').hide();
                $('.mainTwo').show();
                $('.firstSet li').eq(1).addClass('active').siblings().removeClass('active');
				 $('.firstSet li').eq(1).parents(".firstSetMain").find("h1").text("发送消息");
            }); 
			 var counter = 5;  
			var interval;
            $('.markBtn').click(function() {
                $('.mainTwo').hide();
                $('.mainThree').show();
				$('.firstSet li').eq(1).addClass('active').siblings().removeClass('active');
                $('.firstSet li').eq(2).addClass('active').siblings().removeClass('active');
				$('.firstSet li').eq(2).parents(".firstSetMain").find("h1").text("轨迹回放");

                //script for timer
              
              interval = setInterval(function() {
					 
                    counter--;
                    $('.fivz').text(counter);
                    if (counter == 0) {
                         $('.firstSet li').eq(3).addClass('active').siblings().removeClass('active');
                         $('.mainThree').hide();
                         $('.MapPop').fadeOut();
						 $('.firstSet li').eq(0).addClass('active').siblings().removeClass('active');
                        clearInterval(interval);
						counter=5;
						  $('.fivz').text("5");
                    }
                }, 1000);
            });
            $('.close').click(function(e) {
				$('.firstSet li').eq(0).addClass('active').siblings().removeClass('active');
                $('.MapPop').fadeOut();
				$(".statusPopOne").fadeOut();
				$(".btn").removeClass("grnBtn");
				$('.mainTwo').css("display","none");
				 counter = 5;
				  clearInterval(interval);
				    $('.fivz').text("5");
				//var counter = 5;
				
            });
			/*mappopup starts*/	
			/*pop up hide starts*/
			 $(window).click(function(e) {
				var target =$(event.target);
				   if(target.is('.MapPop')){
						target.hide();
						$('.firstSet li').eq(0).addClass('active').siblings().removeClass('active');
						 counter = 5;
						   clearInterval(interval);
						     $('.fivz').text("5");
				   }
				   if(target.is('.statusPopOne')){
					   $(".btn").removeClass("grnBtn");
						target.hide();
				   }
			});
			/*pop up hide ends*/
		});