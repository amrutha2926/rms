<!--
 // **************************************************
 // ******* Name: drora
 // ******* Description: Bootstrap 4 Admin Dashboard
 // ******* Version: 1.0.0
 // ******* Released on 2019-02-08 15:41:24
 // ******* Support Email : quixlab.com@gmail.com
 // ******* Support Skype : sporsho9
 // ******* Author: Quixlab
 // ******* URL: https://quixlab.com
 // ******* Themeforest Profile : https://themeforest.net/user/quixlab
 // ******* License: ISC
 // ***************************************************
-->

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.themefisher.com/drora/main/template/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 17:32:56 GMT -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<!--<script>
    $(document).ready(function(){
        $("#exampleModal").modal('show');
    });
</script> -->

<body>

    <!--*******************
        Preloader start
    ********************-->

    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr">D</b>
                    <span class="brand-title"><b>Drora</b></span>
                </a>
            </div>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="toggle-icon"><i class="icon-menu"></i></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        @include('header')
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">


            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-between mb-3">
					<div class="col-12 text-left">
						<h2 class="page-heading">Hi,Welcome Back!</h2>

					</div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h3 class="content-heading">MENU MASTER</h3>
                    </div>


                    <div class="col-xl-6 col-xxl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add Menu</h4>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="float:Right;margin-bottom: 10px;">Add Menu +</button>
                                <!-- Modal -->
                                <!-- Button trigger modal -->
                                <!-- Modal -->
                                <div class="modal fade table-modal" id="exampleModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="width: 1000px;">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add Menu Rate</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="basic-form">
                                                  <form method="post" action="{{route('menu.add')}}">
                                                        @csrf
                                                        <!--<div class="form-row">
                                                            <div class="form-group col-md-2">
                                                                <label>Select area</label>
                                                                <select id="inputState" class="form-control">
                                                                    <option selected>Choose...</option>
                                                                    <option>Option 1</option>
                                                                    <option>Option 2</option>
                                                                    <option>Option 3</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <label>Menu Name</label>
                                                                <input type="text" class="form-control" placeholder="Menu Name">
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <label>Single</label>
                                                                <select id="inputState" class="form-control">
                                                                    <option selected>Choose...</option>
                                                                    <option>Option 1</option>
                                                                    <option>Option 2</option>
                                                                    <option>Option 3</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-1">
                                                                <label>Rate</label>
                                                                <input type="text" class="form-control" placeholder="Rate">
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <label>Barcode</label>
                                                                <input type="text" class="form-control" placeholder="">
                                                            </div>
                                                            <div class="form-group col-md-1">
                                                                <label>Gst</label>
                                                                <input type="text" class="form-control" placeholder="Gst">
                                                            </div>
                                                            <div class="form-group col-md-2" style="top: 28px;">
                                                                <label></label>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>

                                                        </div>-->
                                                    <div class="col-lg-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="custom-tab-2">
                                                                    <ul class="nav nav-tabs">
                                                                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#success2">Dining</a>
                                                                        </li>
                                                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#info2">Take away</a>
                                                                        </li>
                                                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#danger2">Sample-1</a>
                                                                        </li>
                                                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#warning2">Sample-2</a>
                                                                        </li>
                                                                    </ul>
                                                                    <div class="tab-content tab-content-default">
                                                                        <div class="tab-pane fade show active" id="success2" role="tabpanel">
                                                                            <div>
                                                                                <div class="form-row">
                                                            <div class="form-group col-md-2">
                                                                <label>Select area</label>
                                                                <select id="modes" class="form-control" name="Mode_name">
                                                                  <option selected>Choose...</option>
                                                                  @foreach($modes as $mode)
                                                                <option value="{{ $mode->Mode_name}}">{{ $mode->Mode_name}}</option>
                                                                @endforeach

                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <label>Menu Name</label>
                                                                <input type="text" class="form-control" placeholder="Menu Name" id="menu" name="menu_name">
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <label>Portion</label>
                                                                <select id="portion" class="form-control" name="portion_name">
                                                                  <option selected>Choose...</option>
                                                                  @foreach($portions as $portion)
                                                                <option value="{{ $portion->portion_name}}">{{ $portion->portion_name}}</option>
                                                                @endforeach


                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-1">
                                                                <label>Rate</label>
                                                                <input type="text" class="form-control" placeholder="Rate" id="rate" name="rate">
                                                            </div>
                                                            <div class="form-group col-md-2">
                                                                <label>Barcode</label>
                                                                <input type="text" class="form-control" placeholder="" id="barcode" name="Barcode">
                                                            </div>
                                                            <div class="form-group col-md-1">
                                                                <label>Gst</label>
                                                                <input type="text" class="form-control" placeholder="Gst">
                                                            </div>
                                                            <div class="form-group col-md-2" style="top: 28px;">
                                                                <label></label>
                                                                <button type="button" class="btn btn-primary" class="add-row">Save changes</button>
                                                            </div>

                                                        </div>
                                                                                <div class="col-xl-6 col-xxl-12">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-responsive-sm" id="tblCustomers">
                                                                    <thead>
                                                                        <tr>

                                                                            <th>Mode</th>
                                                                            <th>Menu Name</th>
                                                                            <th>Portion name</th>
                                                                            <th>Price</th>
                                                                            <th>GST</th>
                                                                            <th>Barcode</th>
                                                                            <th>Edit/Delete</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>

                                                                            <td><select id="modes" class="form-control" name="Mode_name">
                                                                                <option selected>Choose...</option>
                                                                                @foreach($modes as $mode)
                                                                              <option value="{{ $mode->Mode_name}}">{{ $mode->Mode_name}}</option>
                                                                              @endforeach

                                                                              </select></td>
                                                                            <td><input type="text" class="form-control" placeholder="Menu Name" id="menu" name="menu_name">
                                                                            </td>
                                                                            <td><select id="portion" class="form-control" name="portion_name">
                                                                              <option selected>Choose...</option>
                                                                              @foreach($portions as $portion)
                                                                            <option value="{{ $portion->portion_name}}">{{ $portion->portion_name}}</option>
                                                                            @endforeach


                                                                            </select></td>
                                                                            <td ><input type="text" class="form-control" placeholder="Rate" id="rate" name="rate"></td>
                                                                            <td ><input type="text" class="form-control" placeholder="Rate" id="gst" name="rate"></td>
                                                                            <td ><input type="text" class="form-control" placeholder="Rate" id="barcode" name="rate"></td>
                                                                            <td><input type="button" onclick="Add()" value="Add" /></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade" id="info2">
                                                                            <div>
                                                                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade" id="danger2">
                                                                            <div>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade" id="warning2">
                                                                            <div>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <!-- Button trigger modal -->
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                        <thead>
                                            <tr>
                                                <th scope="col">Task</th>
                                                <th scope="col">Progress</th>
                                                <th scope="col">Deadline</th>
                                                <th scope="col">Label</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Air Conditioner</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(127, 99, 244, .1)">
                                                        <div class="progress-bar bg-primary" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>Apr 20,2018</td>
                                                <td><span class="badge badge-primary">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Textiles</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(76, 175, 80, .1)">
                                                        <div class="progress-bar bg-success" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>May 27,2018</td>
                                                <td><span class="badge badge-success">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Milk Powder</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(70, 74, 83, .1)">
                                                        <div class="progress-bar bg-dark" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>May 18,2018</td>
                                                <td><span class="badge badge-dark">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Boats</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(255, 193, 7, .1)">
                                                        <div class="progress-bar bg-warning" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>Jun 28,2018</td>
                                                <td><span class="badge badge-warning">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed & Developed by <a href="" target="_blank">otlet.in</a> 2020</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->



    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{asset('app_assets/assets/plugins/common/common.min.js')}}"></script>
    <script src="{{asset('app_assets/js/custom.min.js')}}"></script>
    <script src="{{asset('app_assets/js/settings.js')}}"></script>
    <script src="{{asset('app_assets/js/quixnav.js')}}"></script>
    <script src="{{asset('app_assets/js/styleSwitcher.js')}}"></script>

    <!-- JS Grid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('app_assets/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{asset('app_assets/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{asset('app_assets/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>


    <!-- JS Grid Init -->
    <script src="{{asset('app_assets/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/datatables.init.js')}}"></script>

    <!-- <script>
        $(document).ready(function(){
          //  $("#exampleModal").modal('show');
            $(".add-row").click(function(){
                var modes = $("#modes").val();
                var mnenu = $("#menu").val();
                var portion=$("#portion").val();
                var rate=$("#rate").val();
                var barcode=$("#barcode").val();
                var markup = "<tr><td>" + modes + "</td><td>" + mnenu + "</td> <td>" + portion + "</td> <td>" + rate + "</td> <td>" + barcode + "</td> </tr>";
                $("table tbody").append(markup);
            });

            // Find and remove selected table rows
            $(".delete-row").click(function(){
                $("table tbody").find('input[name="record"]').each(function(){
                	if($(this).is(":checked")){
                        $(this).parents("tr").remove();
                    }
                });
            });
        });
    </script> -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript">


            function Add() {
                AddRow($("#modes").val(), $("#menu").val(),$("#portion").val(),$("#rate").val(),$("#gst").val(),$("#barcode").val());
                $("#modes").val("");
                $("#menu").val("");
                  $("#portion").val("");
                    $("#rate").val("");
                    $("#gst").val("");
                      $("#barcode").val("");
            };

            function AddRow(modes, menu,portion,rate,gst,barcode) {
                //Get the reference of the Table's TBODY element.
                var tBody = $("#tblCustomers > TBODY")[0];

                //Add Row.
                row = tBody.insertRow(-1);

                //Add Name cell.
                var cell = $(row.insertCell(-1));
                cell.html(modes);

                //Add Country cell.
                cell = $(row.insertCell(-1));
                cell.html(menu);
                cell = $(row.insertCell(-1));
                cell.html(portion);
                cell = $(row.insertCell(-1));
                cell.html(rate);
                cell = $(row.insertCell(-1));
                cell.html(gst);
                cell = $(row.insertCell(-1));
                cell.html(barcode);

                //Add Button cell.
                cell = $(row.insertCell(-1));
                var btnRemove = $("<input />");
                btnRemove.attr("type", "button");
                btnRemove.attr("onclick", "Remove(this);");
                btnRemove.val("Remove");
                cell.append(btnRemove);
            };

            function Remove(button) {
                //Determine the reference of the Row using the Button.
                var row = $(button).closest("TR");
                var name = $("TD", row).eq(0).html();
                if (confirm("Do you want to delete: " + name)) {

                    //Get the reference of the Table.
                    var table = $("#tblCustomers")[0];

                    //Delete the Table row using it's Index.
                    table.deleteRow(row[0].rowIndex);
                }
            };
        </script>
</body>


<!-- Mirrored from demo.themefisher.com/drora/main/template/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 17:32:58 GMT -->
</html>
