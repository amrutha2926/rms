<!--
 // **************************************************
 // ******* Name: drora
 // ******* Description: Bootstrap 4 Admin Dashboard
 // ******* Version: 1.0.0
 // ******* Released on 2019-02-08 15:41:24
 // ******* Support Email : quixlab.com@gmail.com
 // ******* Support Skype : sporsho9
 // ******* Author: Quixlab
 // ******* URL: https://quixlab.com
 // ******* Themeforest Profile : https://themeforest.net/user/quixlab
 // ******* License: ISC
 // ***************************************************
-->

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.themefisher.com/drora/main/template/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 17:32:56 GMT -->


<body>

    <!--*******************
        Preloader start
    ********************-->

    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr">D</b>
                    <span class="brand-title"><b>Drora</b></span>
                </a>
            </div>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="toggle-icon"><i class="icon-menu"></i></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        @include('header')
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">


            <!-- row -->

            <div class="container-fluid">
                <div class="row justify-content-between mb-3">
					<div class="col-12 text-left">
						<h2 class="page-heading">Hi,Welcome Back!</h2>

					</div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h3 class="content-heading">MENU MASTER</h3>
                    </div>


                    <div class="col-xl-6 col-xxl-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add Menu</h4>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong" style="float:Right;margin-bottom: 10px;">Add Menu +</button>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalLong">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Add Menu Master</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="basic-form">
                                                    <form method="post" action="{{route('menu.add')}}">
                                                          @csrf
                                                        <div class="form-row">
                                                            <div class="form-group col-md-6">
                                                                <label>Menu Name</label>
                                                                <input type="text" class="form-control" id="text1" placeholder="Menu Name" name="menu_name">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Item Code</label>
                                                                <input type="text" class="form-control" placeholder="Item Code" name="item_code">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Name in Bill</label>
                                                                <input type="text" id="text2"class="form-control" placeholder="Name in Bill" name="name_in_bill">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Kitchen Type</label>
                                                                <select id="inputState" class="form-control" name="kitchen_type">
                                                                  @foreach($kitchens as $kitchen)
        <option value="{{ $kitchen->kitchen_type}}">{{ $kitchen->kitchen_type}}</option>
       @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Main Categories</label>
                                                                <select id="inputState" class="form-control" name="category_name">
                                                                  @foreach($categories as $category)
        <option value="{{ $category->category_name}}">{{ $category->category_name}}</option>
       @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Rate Type</label>
                                                                <select id="inputState" class="form-control" name="portion_name">
                                                                  @foreach($portions as $portion)
                                                                <option value="{{ $portion->portion_name}}">{{ $portion->portion_name}}</option>
                                                                @endforeach



                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <label>Description</label>
                                                                <textarea class="form-control" rows="4" id="comment" name="Description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" value="active" name="status" checked>Active
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" value="active" name="Adds_on">Add-ons
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" value="active" name="stock">Stock
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" name="Dynamic_rate" value="active">Dynamic Rate
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" name="show_in_kot" value="active" checked>Show In Kot
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" name="print_kot" value="active" checked>Print Kot
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" name="except_tax" value="active" checked>Except tax
                                                                </label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label">
                                                                    <input type="checkbox" class="form-check-input" name="Barcode" value="active" checked>Manual Barcode
                                                                </label>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <input type="submit" class="btn btn-primary" class="btn btn-primary"   value="Save changes">
                                                        </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- Button trigger modal -->

                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped verticle-middle table-responsive-sm">
                                        <thead>
                                            <tr>
                                                <th scope="col">Task</th>
                                                <th scope="col">Progress</th>
                                                <th scope="col">Deadline</th>
                                                <th scope="col">Label</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Air Conditioner</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(127, 99, 244, .1)">
                                                        <div class="progress-bar bg-primary" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>Apr 20,2018</td>
                                                <td><span class="badge badge-primary">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Textiles</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(76, 175, 80, .1)">
                                                        <div class="progress-bar bg-success" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>May 27,2018</td>
                                                <td><span class="badge badge-success">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Milk Powder</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(70, 74, 83, .1)">
                                                        <div class="progress-bar bg-dark" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>May 18,2018</td>
                                                <td><span class="badge badge-dark">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Boats</td>
                                                <td>
                                                    <div class="progress" style="background: rgba(255, 193, 7, .1)">
                                                        <div class="progress-bar bg-warning" style="width: 70%;" role="progressbar"><span class="sr-only">70% Complete</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>Jun 28,2018</td>
                                                <td><span class="badge badge-warning">70%</span>
                                                </td>
                                                <td><span><a href="javascript:void()" class="mr-4" data-toggle="tooltip" data-placement="top" title="Edit"><i
                                                                class="fa fa-pencil color-muted"></i> </a><a href="javascript:void()"
                                                            data-toggle="tooltip" data-placement="top" title="Close"><i
                                                                class="fa fa-close color-danger"></i></a></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->


        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright © Designed & Developed by <a href="" target="_blank">otlet.in</a> 2020</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->



    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="{{asset('app_assets/assets/plugins/common/common.min.js')}}"></script>
    <script src="{{asset('app_assets/js/custom.min.js')}}"></script>
    <script src="{{asset('app_assets/js/settings.js')}}"></script>
    <script src="{{asset('app_assets/js/quixnav.js')}}"></script>
    <script src="{{asset('app_assets/js/styleSwitcher.js')}}"></script>

    <!-- JS Grid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('app_assets/assets/plugins/jsgrid/js/jsgrid.min.js')}}"></script>
    <!-- Footable -->
    <script src="{{asset('app_assets/assets/plugins/footable/js/footable.min.js')}}"></script>
    <!-- Bootgrid -->
    <script src="{{asset('app_assets/assets/plugins/jquery-bootgrid/dist/jquery.bootgrid.min.js')}}"></script>
    <!-- Datatable -->
    <script src="{{asset('app_assets/assets/plugins/datatables/js/jquery.dataTables.min.js')}}"></script>


    <!-- JS Grid Init -->
    <script src="{{asset('app_assets/js/plugins-init/jsgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/footable-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/jquery.bootgrid-init.js')}}"></script>
    <script src="{{asset('app_assets/js/plugins-init/datatables.init.js')}}"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#text1').keyup(function(){
            $('#text2').val($(this).val());
          });
        });
    </script>


</body>


<!-- Mirrored from demo.themefisher.com/drora/main/template/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Dec 2019 17:32:58 GMT -->
</html>
