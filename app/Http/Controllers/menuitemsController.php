<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class menuitemsController extends Controller
{
  public function add_p(Request $request)
{
    $MenuName = $request->input('MenuName');
      $ItemCode=$request->input('ItemCode');
	  $NameinBill = $request->input('NameinBill');
      $Kitchen=$request->input('Kitchen');
	  $MainCategory = $request->input('MainCategory');
      $Diet=$request->input('Diet');
	  $EstTime = $request->input('EstTime');
      $PrepeMode=$request->input('PrepeMode');
	  $RateType = $request->input('RateType');
      $BaseUnit=$request->input('BaseUnit');
	  $UnitType = $request->input('UnitType');
      $Description=$request->input('Description');
	  $Active = $request->input('Active');
      $AddOns=$request->input('AddOns');
	  $Stock = $request->input('Stock');
      $DynamicRate=$request->input('DynamicRate');
	  $StockNumbers = $request->input('StockNumbers');
      $ShowinKod=$request->input('ShowinKod');
	  $ExemptTax = $request->input('ExemptTax');
      $PrintKot=$request->input('PrintKot');
	  $ManualBarcode = $request->input('ManualBarcode');
        DB::table('tbl_menu')->insert(array(
          array('MenuName' => $MenuName,'Itemcode'=>$ItemCode,
		  'NameinBil' => $NameinBill,'Kitchen'=>$Kitchen,
		  'Maincategory' => $MainCategory,'Diet'=>$Diet,
		  'EstTime' => $EstTime,'PrepeMode'=>$PrepeMode,
		  'RateType' => $RateType,'BaseUnit'=>$BaseUnit,
		  'UnitType' => $UnitType,'Description'=>$Description,
		  'Active' => $Active,'AddOns'=>$AddOns,
		  'Stock' => $Stock,'DynamicRate'=>$DynamicRate,
		  'StockinNumbers' => $StockNumbers,'ShowinKod'=>$ShowinKod,
		  'ExemptTax' => $ExemptTax,'PrintinKot'=>$PrintKot,
		  'ManualBarcode' => $ManualBarcode
		  ),

      ));
      $users = DB::select('select * from tbl_menu');
      return view('menuitems',['users'=>$users]);
    }
    public function show_p()
    {
      $users = DB::select('select * from tbl_menu');
      return view('menuitems',['users'=>$users]);
    }
    public function display_p($id) {
          $users = DB::select('select * from tbl_menu where id = ?',[$id]);
          return view('menuitems',['users'=>$users]);
       }
       public function edit_p(Request $request,$id) {
          $MenuName = $request->input('MenuName');
      $ItemCode=$request->input('ItemCode');
	  $NameinBill = $request->input('NameinBill');
      $Kitchen=$request->input('Kitchen');
	  $MainCategory = $request->input('MainCategory');
      $Diet=$request->input('Diet');
	  $EstTime = $request->input('EstTime');
      $PrepeMode=$request->input('PrepeMode');
	  $RateType = $request->input('RateType');
      $BaseUnit=$request->input('BaseUnit');
	  $UnitType = $request->input('UnitType');
      $Description=$request->input('Description');
	  $Active = $request->input('Active');
      $AddOns=$request->input('AddOns');
	  $Stock = $request->input('Stock');
      $DynamicRate=$request->input('DynamicRate');
	  $StockNumbers = $request->input('StockNumbers');
      $ShowinKod=$request->input('ShowinKod');
	  $ExemptTax = $request->input('ExemptTax');
      $PrintKot=$request->input('PrintKot');
	  $ManualBarcode = $request->input('ManualBarcode');
          DB::update('update tbl_menu set
MenuName=?,Itemcode=?,
		  NameinBil=?,Kitchen=?,
		  Maincategory=?,Diet=?,
		  EstTime =?,PrepeMode=?,
		  RateType=?,BaseUnit=?,
		  UnitType=?,Description=?,
		  Active=?,AddOns=?,
		  Stock=?,DynamicRate=?,
		  StockinNumbers=?,ShowinKod=?,
		  ExemptTax =?,PrintinKot=?,
		  ManualBarcode =?
		  where id = ?',[$MenuName, $ItemCode,
	  $NameinBill,
      $Kitchen,
	  $MainCategory,
      $Diet,
	  $EstTime ,
      $PrepeMode,
	  $RateType,
      $BaseUnit,
	  $UnitType,
      $Description,
	  $Active ,
      $AddOns,
	  $Stock,
      $DynamicRate,
	  $StockNumbers ,
      $ShowinKod,
	  $ExemptTax ,
      $PrintKot,
	  $ManualBarcode,$id]);
        $users = DB::select('select * from tbl_menu');
           return view('menuitems',['users'=>$users]);
       }
       public function destroy_p($id) {
          DB::delete('delete from tbl_menu where id = ?',[$id]);
          $users = DB::select('select * from tbl_menu');
          return view('menuitems',['users'=>$users]);
      echo "Record deleted successfully.<br/>";
        echo '<a href = "/menuitems">Click Here</a> to go back.';
       }
    //
}
