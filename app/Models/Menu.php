<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
  protected $primaryKey = 'menu_id';
protected $fillable = [
    'menu_name', 'item_code','name_in_bill','kitchen_type','category_name','portion_name','Description','status','Adds_on','stock','Dynamic_rate',
    'show_in_kot','print_kot','except_tax','Barcode',
];
    //
}
