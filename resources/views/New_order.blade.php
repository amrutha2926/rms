<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="{{asset('order/css/style.css')}}" />
<title>Display</title>
<style>
a:focus
{
    background:green;
}
</style>
</head>

<body>
        <section class="top-headd">
                <div class="wid">
                <a href="" class="log-top">Logo</a>
                <div class="fr">
                    <ul>
                        <li><a href="" title="Home"><img src="{{asset('order/images/home.png')}}"></a></li>
                        <li><a href="" title="Back"><img src="{{asset('order/images/reply.png')}}"></a></li>
                        <li><a href="" title="Settings"><img src="{{asset('order/images/settings-gears.png')}}"></a></li>
                    </ul>
                </div>
                </div>

        </section>
<section class="all-one">
<div class="wid">
<div class="order-type_left">
<ul class="orderUlF1">
<li><a href="#" class="active"><span>Dine In</span></a></li>
<li><a href="#"><span>Delivery</span></a></li>
<li><a href="#"><span>Swiggy</span></a></li>
<li><a href="#"><span>Zomato</span></a></li>
<li><a href="#"><span>Take Away</span></a></li>
<li><a href="#"><span>Order Type</span></a></li>
</ul>

<ul class="orderUlF2">
<h2>table No</h2>
@foreach($tables as $table)
{
<li><a href="" class="persons" data-nop="{{ $table->seats}}">{{$table->table_no}}</a></li>
}
@endforeach
</ul>

<ul class="orderUlF2">
<h2>No Of Persons</h2>
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#">6</a></li>
<li><a href="#">7</a></li>
<li><a href="#">8</a></li>
<li><a href="#">9</a></li>
<li><a href="#">0</a></li>
<li><a href="#">+</a></li>
</ul>
</div>
<div class="order-type_center">
<h2><a href="#">NEW ORDER <span>+</span></a></h2>
<table class="tableClCtr" width="100%">
<tr>
<td width="7%">1</td>
<td width="25%">Chicken tandoori</td>
<td width="15%">Full</td>
<td width="18%">AED <strong>28.85</strong></td>
<td width="20%"><textarea>Notes</textarea></td>
<td width="5%"><a href="#" class="editbt"><img src="{{asset('order/images/edit.png')}}" alt="" /></a></td>
<td width="5%"><a href="#" class="dltbt"><img src="{{asset('order/images/dltico.png')}}" alt="" /></a></td>
<td width="5%"><a href="#" class="donebt"><img src="{{asset('order/images/tic.png')}}" alt="" /></a></td>
</tr>
<tr>
<td width="7%">2</td>
<td width="25%">kerala parotta</td>
<td width="15%">8 Nos</td>
<td width="18%">AED <strong>6.18</strong></td>
<td width="20%"><textarea>Notes</textarea></td>
<td width="5%"><a href="#" class="editbt"><img src="images/edit.png" alt="" /></a></td>
<td width="5%"><a href="#" class="dltbt"><img src="images/dltico.png" alt="" /></a></td>
<td width="5%"><a href="#" class="donebt"><img src="images/tic.png" alt="" /></a></td>
</tr>
<tr>
<td width="7%">3</td>
<td width="25%">chilli chicken dry</td>
<td width="15%">Half</td>
<td width="18%">AED <strong>13.12</strong></td>
<td width="20%"><textarea>Notes</textarea></td>
<td width="5%"><a href="#" class="editbt"><img src="images/edit.png" alt="" /></a></td>
<td width="5%"><a href="#" class="dltbt"><img src="images/dltico.png" alt="" /></a></td>
<td width="5%"><a href="#" class="donebt"><img src="images/tic.png" alt="" /></a></td>
</tr>

<tr>
<td width="7%">4</td>
<td width="25%">Tea</td>
<td width="15%">4</td>
<td width="18%">AED <strong>5</strong></td>
<td width="20%"><textarea>Notes</textarea></td>
<td width="5%"><a href="#" class="editbt"><img src="images/edit.png" alt="" /></a></td>
<td width="5%"><a href="#" class="dltbt"><img src="images/dltico.png" alt="" /></a></td>
<td width="5%"><a href="#" class="donebt"><img src="images/tic.png" alt="" /></a></td>
</tr>
<tfoot>
<tr>
<td colspan="2" width="50%"><input type="button"  value="Confirm"/ class="ftbtnsx"></td>
<td colspan="2" width="50%"><input type="button"  value="Print Bill" class="ftbtnsx"/></td>
</tr>
<tr>
<td width="50%"><input type="button"  value="Settle Bill" class="ftbtnsx"/></td>
<td width="50%"><input type="button"  value="Customer Details" class="ftbtnsx"/></td>
</tr>
<tr>
<td width="50%"><input type="button"  value="Cancel Bill" class="ftbtnsx"/></td>
<td width="50%"><input type="button"  value="Reprint Bill" class="ftbtnsx"/></td>
</tr>

</tfoot>
</table>
</div>
<div class="order-type_right">
<h2>Bill</h2>
<ul class="kotno">
<li><a href="#">Kot No 1</a></li>
<li><a href="#">Kot No 2</a></li>
</ul>

<table class="tableCl1" width="100%">
<tr>
<td width="5%">1</td>
<td width="50%">Chicken tandoori</td>
<td width="20%">Full</td>
<td width="25%" align="right">AED <strong>28.85</strong></td>
</tr>
<tr>
<td width="5%">2</td>
<td width="50%">kerala parotta</td>
<td width="20%">8 Nos</td>
<td width="25%" align="right">AED <strong>6.18</strong></td>
</tr>
<tr>
<td width="5%">3</td>
<td width="50%">chilli chicken dry</td>
<td width="20%">Half</td>
<td width="25%" align="right" >AED <strong>13.12</strong></td>
</tr>

<tr>
<td width="5%">4</td>
<td width="50%">Tea</td>
<td width="20%">4</td>
<td width="25%" align="right">AED <strong>5</strong></td>
</tr>
<tfoot>
<tr>
<td colspan="2" width="50%">TOTAL</td>
<td colspan="2" width="50%" align="right" style="font-size:18px">AED <strong>53.5</strong></td>
</tr>
<tr>
<td width="70%"><span>(%) <input type="text" /></span>
<span>(AED) <input type="text" /></span>
</td>
<td width="30%" align="right"><input type="submit"  value="submit"/></td>
</tr>
<tr>
<td colspan="2" width="50%">GRAND TOTAL</td>
<td colspan="2" width="50%" align="right" style="font-size:18px">AED <strong>53.5</strong></td>
</tr>
</tfoot>
</table>

</div>
</div>
</section>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function () {
    $(".persons").click(function () {
        var per = $(this).data("nop");
        alert(per);
    });
});

</script>
</body>
</html>
