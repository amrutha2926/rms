<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kitchen;
use App\Models\Category;
use App\Models\Portion;
use App\Models\Mode;
use App\Models\Menu;
class MenuaddController
{
    //
public function index()
{
  $kitchens= Kitchen::select('kitchen_type')->get();

     $categories= Category::select('category_name')->get();
     $portions=Portion::select('portion_name')->get();
     $modes=Mode::select('Mode_name')->get();
      return view('menuadd',compact('kitchens','categories','portions','modes'));

}
public function add(Request $request)
{
 Menu::create($request->except('_token'));
 return redirect('/Menurate');
}



}
