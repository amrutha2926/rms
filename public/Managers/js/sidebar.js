// JavaScript Document
/*leftmenu starts*/

$(document).ready(function() {
    $(".menuOne").addClass("menuOneLink");
    $(".menuListOne .leftText").addClass("menuMainSpan");
    $(".menuListOne").find("ul").hide();
    $(".menu").addClass("menuMain");
    $(".menuOne").addClass("menuOneLink");
    $(".sidebar-icon").click(function() {
        $(".menu").toggleClass("menuMain");
        $(".menuListOne").click(function() {
            $(this).find(".menuOne").toggleClass("active");
            $(this).siblings().find(".menuOne").removeClass("active");
            $(this).find("ul").show();
            $(this).siblings().find("ul").hide();
        }
        );
        $(this).find(".leftMenu").toggleClass("cross");
        $(".menuOne").toggleClass("menuOneLink");
        $(".menuListOne .leftText").toggleClass("menuMainSpan");
        $(".menuListOne").find("ul").hide();
    }
    );
    $(".menuListOne").click(function() {
        $(this).parents(".sidebar-menu").find(".leftMenu").addClass("cross");
        $(".menuOne").removeClass("menuOneLink");
        $(".menu").removeClass("menuMain");
        $(".menuListOne .leftText").removeClass("menuMainSpan");
        $(this).find("ul").show();
        $(this).siblings().find("ul").hide();
        $(this).find(".menuOne").toggleClass("active");
        $(this).siblings().find(".menuOne").removeClass("active");
    }
    );
}
);
$(document).click(function(event) {
    if ( !$(event.target).hasClass('leftSec')) {
        $(".menu").addClass("menuMain");
        $(".sidebar-icon").find(".leftMenu").removeClass("cross");
        $(".menuOne").addClass("menuOneLink");
        $(".menuListOne .leftText").addClass("menuMainSpan");
        $(".menuListOne").find("ul").hide();
        $(".menuIcon").hide();
        $(".menuListOne").find(".menuIcon").removeClass("leftArrow");
    }
}
);
$('li.menuListOne a').click(function(e) {
    $('.menuIcon').show();
    $(this).children('.menuIcon').addClass('leftArrow');
    var arrow=$(this).parent().siblings().children().children('.menuIcon').hasClass('leftArrow');
    if(arrow) {
        $(this).parent().siblings().children().children('.menuIcon').removeClass('leftArrow');
    }
}
);
$('.logoOne').click(function(e) {
    //$('.menuIcon').toggle();
    var cross=$(this).children().children().hasClass('cross');
    if(!cross) {
        $('.menuIcon').hide();
    }
    else {
        $('.menuIcon').show();
    }
}
);
/*leftmenu ends*/