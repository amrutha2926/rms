<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portion extends Model
{
  protected $primaryKey = 'portion_id';
protected $fillable = [
    'portion_name', 'status',
];
    //
}
