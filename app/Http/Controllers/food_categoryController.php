<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
class food_categoryController
{
  public function add(Request $request)
{
  Category::create($request->except('_token'));


      return redirect('/Category');
    }
    public function show()
    {
      $categories = Category::all();
      return view('food_category',compact('categories'));
    }

       public function edit(Request $request) {
         $cid=$request->category_id;
          $c_name = $request->category_name;
          $category=Category::find($cid);
        //  dd($kitchen);
        $category->update($request->except('_token'));
        //  echo "Record updated successfully.<br/>";
        //  echo '<a href = "/kitchen_type">Click Here</a> to go back.';
          return redirect('/Category');
       }
       public function destroy($id) {
          $de=Category::find($id)->delete();
          return redirect('/Category');
        //  echo "Record deleted successfully.<br/>";
        //  echo '<a href = "/reg_view">Click Here</a> to go back.';
       }
    //
}
