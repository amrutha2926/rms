<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menurates', function (Blueprint $table) {
            $table->bigIncrements('rate_id');
            $table->String('menu_id');
            $table->String('Mode_name');
            $table->String('menu_name');
            $table->String('portion_name');
            $table->String('rate');
            $table->String('GST')->nullable();
            $table->String('Barcode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menurates');
    }
}
