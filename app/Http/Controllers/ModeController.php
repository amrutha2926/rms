<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mode;
class ModeController extends Controller
{
  public function add(Request $request)
{
      Mode::create($request->except('_token'));
      return redirect('/Mode');
    }
    public function show()
    {
      $modes=Mode::all();
      return view('mode',compact('modes'));

    }

       public function edit(Request $request) {
          $mid= $request->mode_id;
          $m_name = $request->Mode_name;
        $mode=Mode::find($mid);
          $mode->update($request->except('_token'));

        return redirect('/Mode');
       }
       public function destroy($id) {
          $res=Mode::find($id)->delete();
          return redirect('/Mode');
        //  echo "Record deleted successfully.<br/>";
        //  echo '<a href = "/reg_view">Click Here</a> to go back.';
       }
    //
}
